<?php /*

 conposr_plus_plus
 Copyright (c) ocProducts, 2004-2019

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    conposr_plus_plus
 */

abstract class PHPBeanPage extends Templateable
{
    protected $globalise = true;

    protected $pageMessages = [];
    protected $pageErrors = [];

    public function __construct()
    {
        $this->populateFromEnvironment();
    }

    // Important security filter for any parameters that come from environment that are going to be used as raw JSON in the templates (i.e. mirrored code)
    protected function enforceJson($json, $default = false)
    {
        if ($json === null) {
            return null;
        }

        $struct = json_decode($json, true);
        if (json_last_error() != JSON_ERROR_NONE) {
            if ($default === false) {
                throw new IncorrectParameterException('Invalid JSON');
            }

            return $default;
        }
        $json = json_encode($struct);
        if (json_last_error() != JSON_ERROR_NONE) {
            if ($default === false) {
                throw new IncorrectParameterException('Invalid JSON');
            }

            return $default;
        }
        return $json;
    }

    protected function populateFromEnvironment($propertiesToIgnore = [])
    {
        $request = $_GET + $_POST;
        foreach (array_keys($request) as $property) {
            if ((property_exists($this, $property)) && ($this->$property !== null) && (!in_array($property, $propertiesToIgnore))) {
                switch (gettype($this->$property)) {
                    case 'array':
                        $this->$property = $request[$property];
                        break;

                    case 'boolean':
                        $val = either_param_integer($property, null);
                        $this->$property = ($val === null) ? null : ($val == 1);
                        break;

                    case 'integer':
                        $this->$property = either_param_integer($property, null);
                        break;

                    case 'double':
                        $val = either_param_string($property, null);
                        $this->$property = ($val === null) ? null : floatval($val);
                        break;

                    case 'string':
                        $val = either_param_string($property, null);
                        $this->$property = ($val === null) ? null : trim($val);
                        break;

                    case 'NULL':
                        // Can't handle this - which is why we need to initialise the properties to scalar values
                        break;

                    case 'object':
                        if ($this->$property instanceof DateTime) {
                            $val = either_param_string($property, null);
                            if (empty($val)) {
                                $this->$property = null;
                            } else {
                                $this->$property = DateTimeUtil::dateTimeFromString($val);
                                if ($this->$property === null) {
                                    throw new DatabaseException('Unrecognised date format for ' . $val);
                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    protected function render($title, $tpl, $vars = [])
    {
        $vars += $this->populateToTemplateParams();

        $middle = do_template($tpl, $vars);
        if ($this->globalise) {
            $out = globalise($title, $middle);
        } else {
            $out = $middle;
        }
        $out->evaluate_echo();
    }

    public function addPageMessage($message)
    {
        $this->pageMessages[] = $message;
    }

    public function addPageError($message)
    {
        $this->pageErrors[] = ['FIELD_NAME' => null, 'MESSAGE' => $message];
    }

    public function addFieldError($fieldName, $message)
    {
        $this->pageErrors[] = ['FIELD_NAME' => $fieldName, 'MESSAGE' => $message];
    }
}
